package cn.ms.light.type;

public enum DataType {

	JSON,
	
	XML,
	
	TEXT,
	
	OTHER;
	
}

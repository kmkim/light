package cn.ms.light.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.buffer.ByteBuf;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.reactivex.netty.protocol.http.server.HttpServerRequest;
import io.reactivex.netty.protocol.http.server.HttpServerResponse;
import io.reactivex.netty.protocol.http.server.RequestHandler;
import rx.Observable;

public class HealthCheck implements RequestHandler<ByteBuf, ByteBuf> {

	private Logger logger=LoggerFactory.getLogger(HealthCheck.class);
	
	@Override
	public Observable<Void> handle(HttpServerRequest<ByteBuf> request, HttpServerResponse<ByteBuf> response) {
		logger.info("Health check called.");
        response.setStatus(HttpResponseStatus.OK);
		response.writeString("200");
		return response.close();
	}

}

package cn.ms.light.monitor;

import java.util.concurrent.TimeUnit;

import io.reactivex.netty.metrics.HttpServerMetricEventsListener;

/**
 * HTTP contains all the metrics that are available from TCP. The following metrics are specific to HTTP
 * <br>
 * Request backlog: The number of requests that have been received but not started processing. This is a gauge.<br>
 * Inflight requests: The number of requests that have been started processing but not yet finished processing. This is a gauge.<br>
 * Response Write Times: Time taken to write responses, including headers and content.<br>
 * Request Read Times: Time taken to read a request.<br>
 * Processed Requests: Total number of requests processed. This is a monotonically increasing counter.<br>
 * Failed Requests: Total number of requests for which the request handling failed.<br>
 * Failed response writes: Total number of responses for which the writes failed.<br>
 * 
 * @author lry
 * @version v1.0
 */
public class HttpServerListener extends HttpServerMetricEventsListener {

	
	//$NON-NLS-接收请求$
	/**
	 * 收到新请求
	 */
	@Override
	protected void onNewRequestReceived() {
		
	}
	
	/**
	 * 按请求接收完成
	 * 
	 * @param duration
	 * @param timeUnit
	 */
	@Override
	protected void onRequestReceiveComplete(long duration, TimeUnit timeUnit) {
	
	}

	
	
	//$NON-NLS-接收请求文本$
	/**
	 * 收到请求文本
	 */
	@Override
	protected void onRequestContentReceived() {
	
	}

	/**
	 * 收到请求头
	 */
	@Override
	protected void onRequestHeadersReceived() {
	
	}
	
	
	
	//$NON-NLS-请求处理开始/成功/失败$
	/**
	 * 请求处理开始
	 * @param duration
	 * @param timeUnit
	 */
	@Override
	protected void onRequestHandlingStart(long duration, TimeUnit timeUnit) {
		
	}

	/**
	 * 请求处理成功
	 * @param duration
	 * @param timeUnit
	 */
	@Override
	protected void onRequestHandlingSuccess(long duration, TimeUnit timeUnit) {
	
	}
	
	/**
	 * 请求处理失败
	 * @param duration
	 * @param timeUnit
	 * @param throwable
	 */
	@Override
	protected void onRequestHandlingFailed(long duration, TimeUnit timeUnit, Throwable throwable) {
	
	}


	
	//$NON-NLS-响应文本写开始/成功/失败$
	/**
	 * 响应文本写开始
	 */
	@Override
	protected void onResponseContentWriteStart() {
	
	}
	
	/**
	 * 响应文本写成功
	 * @param duration
	 * @param timeUnit
	 */
	@Override
	protected void onResponseContentWriteSuccess(long duration, TimeUnit timeUnit) {
	
	}
	
	/**
	 * 响应文本写失败
	 * @param duration
	 * @param timeUnit
	 * @param throwable
	 */
	@Override
	protected void onResponseContentWriteFailed(long duration, TimeUnit timeUnit, Throwable throwable) {
		
	}

	
	
	//$NON-NLS-响应头写开始/成功/失败$
	/**
	 * 响应头开始写
	 */
	@Override
	protected void onResponseHeadersWriteStart() {
		
	}
	
	/**
	 * 响应头写成功
	 * @param duration
	 * @param timeUnit
	 */
	@Override
	protected void onResponseHeadersWriteSuccess(long duration, TimeUnit timeUnit) {
		
	}
	
	/**
	 * 响应头写失败
	 * @param duration
	 * @param timeUnit
	 * @param throwable
	 */
	@Override
	protected void onResponseHeadersWriteFailed(long duration, TimeUnit timeUnit, Throwable throwable) {
		
	}

}

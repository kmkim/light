package cn.ms.light.ctrl;

import java.util.Map;

import cn.ms.light.annotation.Ctrl;
import cn.ms.light.annotation.Handler;
import cn.ms.light.entity.User;
import cn.ms.light.type.DataType;
import cn.ms.light.type.ReqType;
import io.netty.buffer.ByteBuf;
import io.reactivex.netty.protocol.http.server.HttpServerRequest;
import io.reactivex.netty.protocol.http.server.HttpServerResponse;

@Ctrl("/helloworld")
public class HellWorldCtrl {

	@Handler("/show")
	public String show(HttpServerRequest<ByteBuf> request, HttpServerResponse<ByteBuf> response) {
		return "这是show";
	}
	
	
	@Handler(value="/test/:age/:name",reqType=ReqType.GET, dataType=DataType.XML)
	public User test(HttpServerRequest<ByteBuf> request, Map<String, String> params, HttpServerResponse<ByteBuf> response) {
		String age=params.get("age");
		String name=params.get("name");
		return new User(name, age, 123);
	}
	
}

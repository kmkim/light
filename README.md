#light
light是基于Netty+Jax-rs来实现了轻量级，高效的RESTFUL。主要用于要求变态轻、变态小的RESTFUL风格的应用。
如果想使用丰富的RESTFUL，不建议使用该项目；如果只是追求高性能的RESTFUL，那么你可以选用该项目。

##使用方法：
###第一步：编写控制层
```
@Ctrl("/helloworld")
public class HellWorldCtrl {
	
    @Handler(value="/test/:age/:name",reqType=ReqType.GET, dataType=DataType.XML)
    public User test(HttpServerRequest<ByteBuf> request, Map<String, String> params, HttpServerResponse<ByteBuf> response) {
        String age=params.get("age");
	String name=params.get("name");
	return new User(name, age, 123);
}	
}
```

###第二步：启动服务
```
public class LightTest {
    public static void main(String[] args) throws Exception {
	Light.INSTANCE.start(8080, true, "page","cn.ms.light");
    }	
}
```

###第三步：打开浏览器输入以下地址
http://localhost:8080/page/helloworld/test/23/zhangsan
